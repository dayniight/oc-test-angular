import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLoves: number;
  @Input() postDate: string;

  constructor() { }

  ngOnInit() {
  }

  loveLess() {
    this.postLoves --;
  }

  loveMore() {
    this.postLoves ++;
  }
}
